﻿namespace NimbleAMSLyrisDataSync
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtOutput = new System.Windows.Forms.TextBox();
            this.btnNewGo = new System.Windows.Forms.Button();
            this.btnSendHeld = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.txtNumDays = new System.Windows.Forms.TextBox();
            this.lblNumDays = new System.Windows.Forms.Label();
            this.btnWelcome = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtOutput
            // 
            this.txtOutput.Location = new System.Drawing.Point(16, 15);
            this.txtOutput.Margin = new System.Windows.Forms.Padding(4);
            this.txtOutput.Multiline = true;
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.Size = new System.Drawing.Size(1199, 646);
            this.txtOutput.TabIndex = 2;
            // 
            // btnNewGo
            // 
            this.btnNewGo.Location = new System.Drawing.Point(673, 670);
            this.btnNewGo.Margin = new System.Windows.Forms.Padding(4);
            this.btnNewGo.Name = "btnNewGo";
            this.btnNewGo.Size = new System.Drawing.Size(100, 28);
            this.btnNewGo.TabIndex = 3;
            this.btnNewGo.Text = "Sync";
            this.btnNewGo.UseVisualStyleBackColor = true;
            this.btnNewGo.Click += new System.EventHandler(this.btnNewGo_Click);
            // 
            // btnSendHeld
            // 
            this.btnSendHeld.Location = new System.Drawing.Point(565, 670);
            this.btnSendHeld.Margin = new System.Windows.Forms.Padding(4);
            this.btnSendHeld.Name = "btnSendHeld";
            this.btnSendHeld.Size = new System.Drawing.Size(100, 28);
            this.btnSendHeld.TabIndex = 3;
            this.btnSendHeld.Text = "Send Held";
            this.btnSendHeld.UseVisualStyleBackColor = true;
            this.btnSendHeld.Click += new System.EventHandler(this.btnSendHeld_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(457, 670);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 28);
            this.button1.TabIndex = 3;
            this.button1.Text = "Segments";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.btnSegments_Click);
            // 
            // txtNumDays
            // 
            this.txtNumDays.Location = new System.Drawing.Point(1115, 673);
            this.txtNumDays.Name = "txtNumDays";
            this.txtNumDays.Size = new System.Drawing.Size(100, 22);
            this.txtNumDays.TabIndex = 4;
            // 
            // lblNumDays
            // 
            this.lblNumDays.AutoSize = true;
            this.lblNumDays.Location = new System.Drawing.Point(1065, 676);
            this.lblNumDays.Name = "lblNumDays";
            this.lblNumDays.Size = new System.Drawing.Size(44, 17);
            this.lblNumDays.TabIndex = 5;
            this.lblNumDays.Text = "Days:";
            // 
            // btnWelcome
            // 
            this.btnWelcome.Location = new System.Drawing.Point(780, 670);
            this.btnWelcome.Name = "btnWelcome";
            this.btnWelcome.Size = new System.Drawing.Size(100, 28);
            this.btnWelcome.TabIndex = 6;
            this.btnWelcome.Text = "Welcome";
            this.btnWelcome.UseVisualStyleBackColor = true;
            this.btnWelcome.Click += new System.EventHandler(this.btnWelcome_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1232, 709);
            this.Controls.Add(this.btnWelcome);
            this.Controls.Add(this.lblNumDays);
            this.Controls.Add(this.txtNumDays);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnSendHeld);
            this.Controls.Add(this.btnNewGo);
            this.Controls.Add(this.txtOutput);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmMain";
            this.Text = "frmMain";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtOutput;
        private System.Windows.Forms.Button btnNewGo;
        private System.Windows.Forms.Button btnSendHeld;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtNumDays;
        private System.Windows.Forms.Label lblNumDays;
        private System.Windows.Forms.Button btnWelcome;
    }
}

