﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using SalesForceClassLibrary;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;

namespace NimbleAMSLyrisDataSync
{
    public partial class frmMain : Form
    {
        internal int _NumberOfDays = 1;

        internal int NumberOfDays
        {
            get
            {
                if (!string.IsNullOrEmpty(txtNumDays.Text) && txtNumDays.Text.Trim().All(char.IsDigit))
                {
                    _NumberOfDays = Convert.ToInt32(txtNumDays.Text.Trim());
                }
                return _NumberOfDays;
            }

            set
            {
                _NumberOfDays = value;
            }
        }

        private const string _sqlTable = "temp_Lyris";
        bool bWindowOutput = false;

        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            this.Text = String.Format("{0} (v{1})", Application.ProductName, Application.ProductVersion);

#if (DEBUG)
            bWindowOutput = false;
#else
            string[] args = Environment.GetCommandLineArgs();
            if (args.Contains("/win"))
            {
                bWindowOutput = true;
            }
#endif


#if (DEBUG)
            //DoLyrisSubscriptionSync(SyncTypes.OnlineSubscriptions);
            //DoLyrisSubscriptionSendHeld();
            //DoLyrisSegmentsMaster();
            //DoLyrisUnholdNimbleHeld();

            //DoLyrisSubscriptionSync(SyncTypes.OnlineSubscriptions);

            //DoWelcomeEmailSend();

            //DoLyrisCEMOSegment();
#else
            if (args.Contains("/sync"))
            {
                DoLyrisSubscriptionSync(SyncTypes.OnlineSubscriptions);
            }

            if (args.Contains("/held"))
            {
                DoLyrisSubscriptionSendHeld();
            }

            if (args.Contains("/unhold"))
            {
                DoLyrisUnholdNimbleHeld();
            }

            if (args.Contains("/segment"))
            {
                DoLyrisSegmentsMaster();
            }

            if (args.Contains("/cemo"))
            {
                DoLyrisCEMOSegment();
            }

            if (args.Contains("/email"))
            {
                DoLyrisSubscriptionSync(SyncTypes.EmailHistory);
            }

            if (args.Contains("/unsubold"))
            {
                DoLyrisUnsubOldEmails();
            }

            if (args.Contains("/welcome"))
            {
                DoWelcomeEmailSend();
            }

            if (args.Contains("/emap"))
            {
                UpdateEmailMap();
            }

            if (args.Contains("/zs"))
            {
                DoDownloadForShashiTemp();
            }

            if (args.Contains("/zsa"))
            {
                DoDownloadForAcademy();
            }

            if (args.Contains("/emailtest"))
            {
                string strArgFrom = "";
                string strArgTo = "";
                foreach (string arg in args)
                {
                    if (arg.StartsWith("/from:", StringComparison.CurrentCultureIgnoreCase))
                    {
                        strArgFrom = arg.ToLower().Replace("/from:", "");
                    }
                    if (arg.StartsWith("/to:", StringComparison.CurrentCultureIgnoreCase))
                    {
                        strArgTo = arg.ToLower().Replace("/to:", "");
                    }
                }
                Console.WriteLine(string.Format("Preparing to send email. From: {0}, To: {1}", strArgFrom, strArgTo));
                if (strArgFrom != "" && strArgTo != "")
                {
                    SendTestEmail(strArgFrom, strArgTo);
                }
            }
#endif

            if (!bWindowOutput)
            {
                if (this.InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(delegate() { this.Dispose(); }));
                }
                else
                {
                    this.Dispose();
                }
            }
        }

        private string GetLogFileTime()
        {
            return "_" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0') + "_" + DateTime.Now.Hour.ToString().PadLeft(2, '0') + DateTime.Now.Minute.ToString().PadLeft(2, '0') + DateTime.Now.Second.ToString().PadLeft(2, '0');
        }

        private void ProcessLog(DebugInfo dbi)
        {
            //Write Log
            if (bWindowOutput)
            {
                txtOutput.Text = dbi.GetLines();
            }
            else
            {
                string strLogMsg = dbi.WriteLogFile();
            }
        }

        enum SyncTypes
        {
            OnlineSubscriptions,
            EmailHistory
        }

        private void DoLyrisSubscriptionSync(SyncTypes st)
        {
            //BEGIN: Lyris Subscription Sync

            string strFileSaveDateTime = GetLogFileTime();
            string strSyncMode = "";
#if (DEBUG)
            string strSyncQuery = "";
            switch(st)
            {
                case SyncTypes.OnlineSubscriptions:
                    strSyncMode = "ByModifiedSubscriptions";
                    strSyncQuery = String.Format("SELECT Account__r.PersonEmail,Product__r.Online_Subscription_Type__c,Product__r.Lyris_List_Name__c,Product__r.Name,Listserv_Delivery_Option__c,Status__c,CreatedDate FROM Online_Subscription__c WHERE Product__r.NU__Entity__c = 'a0Wd0000008yEbZEAU' AND SystemModstamp = LAST_N_DAYS:{0} AND Status__c != 'Held'", NumberOfDays);
                    break;
                case SyncTypes.EmailHistory:
                    strSyncMode = "ByChangedEmailAddress";
                    strSyncQuery = String.Format("SELECT Account__r.PersonEmail,Product__r.Online_Subscription_Type__c,Product__r.Lyris_List_Name__c,Product__r.Name,Listserv_Delivery_Option__c,Status__c,CreatedDate FROM Online_Subscription__c WHERE Product__r.NU__Entity__c = 'a0Wd0000008yEbZEAU' AND Status__c != 'Held' AND Account__c IN (SELECT AccountId FROM AccountHistory WHERE Field = 'PersonEmail' AND CreatedDate = LAST_N_DAYS:{0}) AND Account__r.PersonEmail != 'unknown-address@leadingage.org'", NumberOfDays);
                    break;
            }
            DebugInfo dbi = new DebugInfo(strSyncMode, String.Format(@"\\LeadingAge-db02\c$\temp\DoLyrisSubscriptionSync{0}{1}.txt", strSyncMode, strFileSaveDateTime));
            BulkData.BulkServer ep = new BulkData.BulkServer(
                ConfigurationManager.ConnectionStrings["LyrisDev"].ConnectionString,
                String.Format(@"\\LeadingAge-db02\c$\temp\LyrisNimbleBaseImport{0}.csv", strFileSaveDateTime),
                "LyrisNimbleBaseImport",
                BulkData.BulkTableAction.TRUNCATE,
                strSyncQuery,
                //Below line is an example of isolating the import to a particular account (Remember to also set /sync in the Project -> Properties menu and comment out the above line) for a one-time sync:
                //String.Format("SELECT Account__r.PersonEmail,Product__r.Online_Subscription_Type__c,Product__r.Lyris_List_Name__c,Product__r.Name,Listserv_Delivery_Option__c,Status__c FROM Online_Subscription__c WHERE SystemModstamp = LAST_N_DAYS:{0} AND Status__c != 'Held' AND Account__r.PersonEmail = 'edmond.marchi@schenectadycounty.com'", NumberOfDays),
                new EnterpriseService()
            );
#else
            string strSyncQuery = "";
            switch(st)
            {
                case SyncTypes.OnlineSubscriptions:
                    strSyncMode = "ByModifiedSubscriptions";
                    strSyncQuery = String.Format("SELECT Account__r.PersonEmail,Product__r.Online_Subscription_Type__c,Product__r.Lyris_List_Name__c,Product__r.Name,Listserv_Delivery_Option__c,Status__c,CreatedDate FROM Online_Subscription__c WHERE ((Product__r.NU__Entity__c = 'a0Wd0000008yEbZEAU' AND Product__r.Online_Subscription_Type__c = 'Listserv') OR (Product__r.Online_Subscription_Type__c = 'Advisory Group')) AND SystemModstamp = LAST_N_DAYS:{0} AND Status__c != 'Held'", NumberOfDays);
                    break;
                case SyncTypes.EmailHistory:
                    strSyncMode = "ByChangedEmailAddress";
                    strSyncQuery = String.Format("SELECT Account__r.PersonEmail,Product__r.Online_Subscription_Type__c,Product__r.Lyris_List_Name__c,Product__r.Name,Listserv_Delivery_Option__c,Status__c,CreatedDate FROM Online_Subscription__c WHERE ((Product__r.NU__Entity__c = 'a0Wd0000008yEbZEAU' AND Product__r.Online_Subscription_Type__c = 'Listserv') OR (Product__r.Online_Subscription_Type__c = 'Advisory Group')) AND Status__c != 'Held' AND Account__c IN (SELECT AccountId FROM AccountHistory WHERE Field = 'PersonEmail' AND CreatedDate = LAST_N_DAYS:{0}) AND Account__r.PersonEmail != 'unknown-address@leadingage.org'", NumberOfDays);
                    break;
            }
            DebugInfo dbi = new DebugInfo(strSyncMode, String.Format(@"C:\temp\DoLyrisSubscriptionSync{0}{1}.txt", strSyncMode, strFileSaveDateTime));
            BulkData.BulkServer ep = new BulkData.BulkServer(
                ConfigurationManager.ConnectionStrings["LyrisLive"].ConnectionString,
                String.Format(@"C:\temp\LyrisNimbleBaseImport{0}.csv", strFileSaveDateTime),
                "LyrisNimbleBaseImport",
                BulkData.BulkTableAction.TRUNCATE,
                strSyncQuery,
                //Below line is an example of isolating the import to a particular account (Remember to also set /sync in the Project -> Properties menu and comment out the above line) for a one-time sync:
                //String.Format("SELECT Account__r.PersonEmail,Product__r.Online_Subscription_Type__c,Product__r.Lyris_List_Name__c,Product__r.Name,Listserv_Delivery_Option__c,Status__c FROM Online_Subscription__c WHERE SystemModstamp = LAST_N_DAYS:{0} AND Status__c != 'Held' AND Account__r.PersonEmail = 'edmond.marchi@schenectadycounty.com'", NumberOfDays),
                new EnterpriseService()
            );
#endif
            dbi.AddLine(String.Format("Lyris Subscription Sync ({0}) -- Setting up BulkServer", strSyncMode));

            bool bDoSQL = false;
            for (int i = 1; i <= 3; ++i)
            {
                BulkData.BulkReturn br = BulkData.DownloadBulkDataSalesforceToSQL(
                    ep,
                    new SalesForceClassLibrary.BulkData.BulkSaveAction(
                        delegate(SalesForceClassLibrary.SalesforceEnterprise.sObject o)
                        {
                            string strRetVal = "";
                            SalesForceClassLibrary.SalesforceEnterprise.Online_Subscription__c sfSub = (SalesForceClassLibrary.SalesforceEnterprise.Online_Subscription__c)o;
                            if (sfSub != null && sfSub.Product__r != null && sfSub.Account__r != null && SalesforceValidation.Account.IsLikeAnEmail(sfSub.Account__r.PersonEmail) && sfSub.Status__c != null)
                            {
                                strRetVal = String.Format("{0}{5}{1}{5}{2}{5}{3}{5}{4}", SalesforceValidation.SafeGetObjectField(sfSub.Account__r.PersonEmail), SalesforceValidation.SafeGetObjectField(sfSub.Product__r.Lyris_List_Name__c), SalesForceClassLibrary.CommonFunctions.NimbleTranslate.ToLyris.ValueOfStatus(SalesforceValidation.SafeGetObjectField(sfSub.Status__c)), SalesforceValidation.Listserv.GetDeliveryOptionForLyrisWithDefault(sfSub, SalesforceValidation.Listserv.DeliveryOptions.Immediate_Delivery), sfSub.CreatedDate, BulkData.BulkCSVFieldChar);
                            }
                            return strRetVal;
                        }
                    )
                );
                dbi.AddLine(String.Format("Lyris Subscription Sync ({3}) -- Data Download (Attempt: {2}) ended with SuccessOf({0}) and Message: {1}", br.Success, br.Message, i, strSyncMode));
                if (br.Success)
                {
                    bDoSQL = true;
                    break;
                }
                else
                {
                    Random r = new Random();
                    Thread.Sleep(r.Next(3000, 17000));
                }
            }

            if (bDoSQL)
            {
                //Run sync after data is gathered
                try
                {
#if(DEBUG)
                    SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["LyrisDev"].ConnectionString);
#else
                    SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["LyrisLive"].ConnectionString);
#endif
                    SqlCommand sqlCmd = new SqlCommand("sp_eNewsletter_Lyris_Nimble_Sync", sqlConn);
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.CommandTimeout = 3600;
                    sqlCmd.Connection.Open();
                    sqlCmd.ExecuteNonQuery();
                    sqlCmd.Connection.Close();
                    sqlConn = null;
                }
                catch (SqlException qEx)
                {
                    dbi.AddLine(String.Format("Lyris Subscription Sync ({0}) -- SQL (sp_eNewsletter_Lyris_Nimble_Sync) Sync failed with SQL Error: {1}", strSyncMode, qEx.Message), true);
                }
            }
            else
            {
                dbi.AddLine(String.Format("Lyris Subscription Sync ({0}) -- SQL (sp_eNewsletter_Lyris_Nimble_Sync) Sync did nothing because the bulk download did not succeed.", strSyncMode), true);
            }

            //sp_eNewsletter_Lyris_Nimble_Sync_EmailForWelcome  [SELECT * FROM LyrisNimbleBaseImport WHERE CreatedDate >= DATEADD(day, -1, GETUTCDATE())]


            //Write Log
            ProcessLog(dbi);

            //END: Lyris Subscription Sync
        }

        struct HeldDataBasic
        {
            public string EmailAddress { get; set; }
            public string List { get; set; }
        }

        private void DoLyrisUnholdNimbleHeld()
        {
            string strFileSaveDateTime = GetLogFileTime();
            string strSQLConn = "";
#if (DEBUG)
            strSQLConn = ConfigurationManager.ConnectionStrings["LyrisDev"].ConnectionString;
            DebugInfo dbi = new DebugInfo(String.Format(@"\\LeadingAge-db02\c$\temp\DoLyrisUnholdNimbleHeld{0}.txt", strFileSaveDateTime));
            BulkData.BulkServer ep = new BulkData.BulkServer(
                strSQLConn,
                String.Format(@"\\LeadingAge-db02\c$\temp\DoLyrisUnholdNimbleHeld{0}.csv", strFileSaveDateTime),
                "LyrisUnholdNimbleHeld",
                BulkData.BulkTableAction.DROP,
                "SELECT Id, Account__r.PersonEmail, Product__r.Lyris_List_Name__c FROM Online_Subscription__c WHERE Product__r.NU__Entity__c = 'a0Wd0000008yEbZEAU' AND Status__c = 'Held'",
                new EnterpriseService()
            );
#else
            strSQLConn = ConfigurationManager.ConnectionStrings["LyrisLive"].ConnectionString;
            DebugInfo dbi = new DebugInfo("Lyris Unhold Nimble Held", String.Format(@"C:\temp\DoLyrisUnholdNimbleHeld{0}.txt", strFileSaveDateTime));
            BulkData.BulkServer ep = new BulkData.BulkServer(
                strSQLConn,
                String.Format(@"C:\temp\DoLyrisUnholdNimbleHeld{0}.csv", strFileSaveDateTime),
                "LyrisUnholdNimbleHeld",
                BulkData.BulkTableAction.DROP,
                "SELECT Id, Account__r.PersonEmail, Product__r.Lyris_List_Name__c FROM Online_Subscription__c WHERE ((Product__r.NU__Entity__c = 'a0Wd0000008yEbZEAU' AND Product__r.Online_Subscription_Type__c = 'Listserv') OR (Product__r.Online_Subscription_Type__c = 'Advisory Group')) AND Status__c = 'Held'",
                new EnterpriseService()
            );
#endif
            dbi.AddLine("DoLyrisUnholdNimbleHeld -- Begin");
            bool bQState = true;
            try
            {
                bool bF = false;
                for (int i = 1; i <= 3; i++)
                {
                    BulkData.BulkReturn br = BulkData.DownloadBulkDataSalesforceToSQL(
                        ep,
                        new SalesForceClassLibrary.BulkData.BulkSaveAction(
                            delegate(SalesForceClassLibrary.SalesforceEnterprise.sObject o)
                            {
                                string strRetVal = "";
                                SalesForceClassLibrary.SalesforceEnterprise.Online_Subscription__c sfAccount = (SalesForceClassLibrary.SalesforceEnterprise.Online_Subscription__c)o;
                                if (sfAccount != null && sfAccount.Account__r != null && sfAccount.Product__r != null)
                                {
                                    strRetVal = ep.QueryHelper.GetAllFieldsAsCSV(sfAccount);
                                }
                                return strRetVal;
                            }
                        )
                    );
                    bQState = br.Success;
                    dbi.AddLine(String.Format("DoLyrisUnholdNimbleHeld -- Data Download ({2}) ended with SuccessOf({0}) and Message: {1}", br.Success, br.Message, i));
                    bF = true;
                    if ((br.Message.ToLower().Contains("error in xml")) || (br.Message.ToLower().Contains("operation") && br.Message.ToLower().Contains("time") && br.Message.ToLower().Contains("out")))
                    {
                        bF = false;
                    }
                    if (bF)
                    {
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                dbi.AddLine(String.Format("DoLyrisUnholdNimbleHeld -- Data Download failed with fatal error: {0}", ex.Message.ToString()), true);
            }

            if (bQState)
            {
                List<SalesForceClassLibrary.SalesforceEnterprise.Online_Subscription__c> lUpdate = new List<SalesForceClassLibrary.SalesforceEnterprise.Online_Subscription__c>();

                try
                {
                    SqlConnection sqc = new SqlConnection(strSQLConn);
                    SqlCommand sqcc = new SqlCommand("sp_eNewsletter_Lyris_Nimble_Sync_Unhold", sqc);
                    sqcc.CommandTimeout = 3600;
                    sqcc.CommandType = CommandType.StoredProcedure;
                    sqcc.Connection.Open();
                    SqlDataReader sdrInfo = sqcc.ExecuteReader();
                    if (sdrInfo.HasRows)
                    {
                        while (sdrInfo.Read())
                        {
                            lUpdate.Add(new SalesForceClassLibrary.SalesforceEnterprise.Online_Subscription__c() { Id = sdrInfo["Subs_Id"].ToString(), Status__c = "Active" });
                        }
                    }
                    else
                    {
                        dbi.AddLine("DoLyrisUnholdNimbleHeld -- No data pulled from SQL because all Held items in Nimble are also Held in Lyris.");
                    }
                    sqcc.Connection.Close();
                }
                catch (Exception ex)
                {
                    dbi.AddLine(String.Format("DoLyrisUnholdNimbleHeld -- SQL data pull failed with error: {0}", ex.Message), true);
                }

                if (lUpdate.Count > 0)
                {
                    EnterpriseService es = new EnterpriseService();
                    es.upsert(lUpdate.ToArray()); //the update invocation on the SF webservice itself doesn't seem to do anything...
                    dbi.AddLine("DoLyrisUnholdNimbleHeld -- Records updated in Salesforce.");
                }
                else
                {
                    dbi.AddLine("DoLyrisUnholdNimbleHeld -- No records needed to be updated because all Held items matched.");
                }
            }
            else
            {
                dbi.AddLine("DoLyrisUnholdNimbleHeld -- Data download from Salesforce did not complete successfully.");
            }

            //Write Log
            ProcessLog(dbi);
        }

        private void DoLyrisSubscriptionSendHeld()
        {
            string strFileSaveDateTime = GetLogFileTime();
#if (DEBUG)
            DebugInfo dbi = new DebugInfo(String.Format(@"\\LeadingAge-db02\c$\temp\DoLyrisSubscriptionSendHeld{0}.txt", strFileSaveDateTime));
            string strDatabase = ConfigurationManager.ConnectionStrings["LyrisDev"].ConnectionString;
#else
            DebugInfo dbi = new DebugInfo("Processing Held Emails", String.Format(@"C:\temp\DoLyrisSubscriptionSendHeld{0}.txt", strFileSaveDateTime));
            string strDatabase = ConfigurationManager.ConnectionStrings["LyrisLive"].ConnectionString;
#endif
            dbi.AddLine("DoLyrisSubscriptionSendHeld -- Begin Downloading Records from sp_eNewsletter_Lyris_Nimble_Sync_Held");
            //Online_Subscription_Temp_Held__c
            List<HeldDataBasic> soaHeldEmail = new List<HeldDataBasic>();
            try
            {
                SqlConnection sqlConn = new SqlConnection(strDatabase);
                SqlCommand sqlCmd = new SqlCommand("sp_eNewsletter_Lyris_Nimble_Sync_Held", sqlConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Connection.Open();
                SqlDataReader sqldrInfo = sqlCmd.ExecuteReader();
                if (sqldrInfo.HasRows)
                {
                    while (sqldrInfo.Read())
                    {
                        soaHeldEmail.Add(new HeldDataBasic() { EmailAddress = sqldrInfo["EmailAddr_"].ToString(), List = sqldrInfo["List_"].ToString() });
                    }
                }
                sqlCmd.Connection.Close();
                sqlConn = null;
            }
            catch (Exception ex)
            {
                dbi.AddLine(String.Format("DoLyrisSubscriptionSendHeld -- SQL Error in sp_eNewsletter_Lyris_Nimble_Sync_Held: {0}", ex.Message.ToString()), true);
            }

            if (soaHeldEmail.Count > 0)
            {
                dbi.AddLine("DoLyrisSubscriptionSendHeld -- Begin processing recent held records.");
                string strBaseQuery = "";
                //string strBaseQuery = String.Format("SELECT Id, Status__c, Account__r.PersonEmail FROM Online_Subscription__c WHERE Status__c = 'Active' AND Account__r.PersonEmail IN ({0})", string.Join(",", soaHeldEmail.Select(x => String.Format("'{0}'", x)).ToArray()));
                string QUERY_BODY = "SELECT Id, Status__c, Account__r.PersonEmail, Product__r.Lyris_List_Name__c FROM Online_Subscription__c WHERE ((Product__r.NU__Entity__c = 'a0Wd0000008yEbZEAU' AND Product__r.Online_Subscription_Type__c = 'Listserv') OR (Product__r.Online_Subscription_Type__c = 'Advisory Group')) AND Status__c = 'Active' AND Account__r.PersonEmail IN (";

                List<string> lQueries = new List<string>();
                strBaseQuery = QUERY_BODY;
                string strTmpQ = "";
                strTmpQ = strBaseQuery;

                List<string> uqEmail = soaHeldEmail.Select(x => x.EmailAddress).ToList<string>().Distinct().ToList<string>();
                string sTrap = "";

                foreach (string s in uqEmail)
                {
                    sTrap = s.Replace("'", @"\'");
                    strTmpQ = strTmpQ + String.Format("'{0}',", sTrap);
                    if (strTmpQ.Length > 1000)
                    {
                        strBaseQuery = strBaseQuery + ")";
                        strBaseQuery = strBaseQuery.Replace(",)", ")");
                        lQueries.Add(strBaseQuery);
                        strBaseQuery = QUERY_BODY;
                        strTmpQ = strBaseQuery;
                        strTmpQ = strTmpQ + String.Format("'{0}',", sTrap);
                    }
                    else
                    {
                        strBaseQuery = strBaseQuery + String.Format("'{0}',", sTrap);
                    }
                }
                if (strTmpQ != QUERY_BODY)
                {
                    strTmpQ = strTmpQ + ")";
                    strTmpQ = strTmpQ.Replace(",)", ")");
                    lQueries.Add(strTmpQ);
                }

                dbi.AddLine(String.Format("lQueries Count: {0}\r\n", lQueries.Count));

                List<SalesForceClassLibrary.SalesforceEnterprise.Online_Subscription__c> lUpdate = new List<SalesForceClassLibrary.SalesforceEnterprise.Online_Subscription__c>();
                Querier<SalesForceClassLibrary.SalesforceEnterprise.Online_Subscription__c> q = new Querier<SalesForceClassLibrary.SalesforceEnterprise.Online_Subscription__c>();
                foreach (string sq in lQueries)
                {
                    for (int iX = 1; iX <= 3; iX++)
                    {
                        if (_BuildUpdateList(iX, sq, ref dbi, ref soaHeldEmail, ref lUpdate, ref q))
                        {
                            break;
                        }
                    }
                }

                if (lUpdate.Count > 0)
                {
                    for (int iX = 0; iX < 3; iX++)
                    {
                        if (_doSendHeld(lUpdate, dbi, iX))
                        {
                            break;
                        }
                    }
                }
                else
                {
                    dbi.AddLine("DoLyrisSubscriptionSendHeld -- No recent held records matched e-mail addresses in Salesforce.");
                }
            }
            else
            {
                dbi.AddLine("DoLyrisSubscriptionSendHeld -- No recent Held records to process.");
            }

            //Write Log
            ProcessLog(dbi);
        }

        private bool _BuildUpdateList(int iCount, string sq, ref DebugInfo dbi, ref List<HeldDataBasic> soaHeldEmail, ref List<SalesForceClassLibrary.SalesforceEnterprise.Online_Subscription__c> lUpdate, ref Querier<SalesForceClassLibrary.SalesforceEnterprise.Online_Subscription__c> q)
        {
            bool bRetVal = true;
            try
            {
                dbi.AddLine(String.Format("Processing Query for Upsert:\r\n{0}\r\n", sq));

                List<SalesForceClassLibrary.SalesforceEnterprise.Online_Subscription__c> lq = q.Query(sq);
                if (lq.Count > 0)
                {
                    HeldDataBasic hMatch = new HeldDataBasic();
                    foreach (SalesForceClassLibrary.SalesforceEnterprise.Online_Subscription__c sfSub in lq)
                    {
                        hMatch.EmailAddress = "";
                        hMatch.List = "";
                        if (sfSub.Account__r != null)
                        {
                            hMatch.EmailAddress = sfSub.Account__r.PersonEmail.ToString();
                            if (sfSub.Product__r != null)
                            {
                                hMatch.List = sfSub.Product__r.Lyris_List_Name__c;
                            }
                            //dbi.AddLine(String.Format("E-mail for Upsert: {0}: Final Status: {1}\r\n", sfSub.Account__r.PersonEmail, sfSub.Status__c));
                        }
                        if (!string.IsNullOrEmpty(hMatch.EmailAddress) && !string.IsNullOrEmpty(hMatch.List))
                        {
                            foreach (HeldDataBasic h in soaHeldEmail)
                            {
                                if (h.EmailAddress.ToLower() == hMatch.EmailAddress.ToLower() && h.List.ToLower() == hMatch.List.ToLower())
                                {
                                    sfSub.Status__c = "Held";

                                    dbi.AddLine(String.Format("E-mail for update: {0}; List: {2}; Status: {1}\r\n", sfSub.Account__r.PersonEmail, sfSub.Status__c, hMatch.List));

                                    sfSub.Account__r = null; //Necessary otherwise the update errors out...
                                    sfSub.Product__r = null; //Necessary otherwise the update errors out...
                                    lUpdate.Add(sfSub);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                dbi.AddLine(String.Format("DoLyrisSubscriptionSendHeld -- Try {0} -- Error trying to download data for upsert: {1}", iCount, ex.Message.ToString()), true);
                bRetVal = false;
            }
            return bRetVal;
        }

        private bool _doSendHeld(List<SalesForceClassLibrary.SalesforceEnterprise.Online_Subscription__c> lUpdate, DebugInfo dbi, int iCount)
        {
            bool bRet = true;
            try
            {
                EnterpriseService es = new EnterpriseService();
                es.upsert(lUpdate.ToArray()); //the update invocation on the SF webservice itself doesn't seem to do anything...
                dbi.AddLine("DoLyrisSubscriptionSendHeld -- Records updated in Salesforce.");
            }
            catch (Exception ex)
            {
                bRet = false;
                dbi.AddLine(String.Format("DoLyrisSubscriptionSendHeld -- Try {0} -- Error trying to upsert records: {1}", iCount, ex.Message.ToString()), true);
            }
            return bRet;
        }

        private void DoLyrisUnsubOldEmails()
        {
            string strFileSaveDateTime = GetLogFileTime();
            string strSOQL = "";
            string strSQLServer = "";
#if (DEBUG)
            DebugInfo dbi = new DebugInfo(String.Format(@"\\LeadingAge-db02\c$\temp\DoLyrisUnsubOldEmails{0}.txt", strFileSaveDateTime));
            strSOQL = String.Format("SELECT AccountId, OldValue FROM AccountHistory WHERE Field = 'PersonEmail' AND CreatedDate = LAST_N_DAYS:{0}", NumberOfDays);
            strSQLServer = ConfigurationManager.ConnectionStrings["LyrisDev"].ConnectionString;
#else
            DebugInfo dbi = new DebugInfo("Unsubscribing OldValue Emails", String.Format(@"C:\temp\DoLyrisUnsubOldEmails{0}.txt", strFileSaveDateTime));
            strSOQL = String.Format("SELECT AccountId, OldValue FROM AccountHistory WHERE Field = 'PersonEmail' AND CreatedDate = LAST_N_DAYS:{0}", NumberOfDays);
            strSQLServer = ConfigurationManager.ConnectionStrings["LyrisLive"].ConnectionString;
#endif
            try
            {
                dbi.AddLine("-- Starting Import for Unsub Old Emails");
                dbi.AddLine(String.Format("Using SOQL: {0}", strSOQL));
                EnterpriseService es = new EnterpriseService();
                Querier<SalesForceClassLibrary.SalesforceEnterprise.sObject> q = new Querier<SalesForceClassLibrary.SalesforceEnterprise.sObject>(es);
                List<SalesForceClassLibrary.SalesforceEnterprise.sObject> lq = q.Query(strSOQL);
                if (lq.Count > 0)
                {
                    SqlConnection sqlConn = new SqlConnection(strSQLServer);
                    SqlCommand sqlCmd = new SqlCommand("usp_Unsub_Old_Email", sqlConn);
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    int iRecsProcessed = 0;
                    foreach (SalesForceClassLibrary.SalesforceEnterprise.sObject o in lq)
                    {
                        SalesForceClassLibrary.SalesforceEnterprise.AccountHistory sfAccount = (SalesForceClassLibrary.SalesforceEnterprise.AccountHistory)o;
                        if (sfAccount != null && sfAccount.OldValue != null && !string.IsNullOrEmpty(sfAccount.OldValue.ToString().Trim()) && SalesforceValidation.Account.IsLikeAnEmail(sfAccount.OldValue))
                        {
                            //pass email to sql sp for unsub
                            sqlCmd.Parameters.Clear();
                            sqlCmd.Parameters.AddWithValue("@unsub_email", sfAccount.OldValue.ToString());
                            sqlCmd.Connection.Open();
                            sqlCmd.ExecuteNonQuery();
                            sqlCmd.Connection.Close();

                            iRecsProcessed++;
                            dbi.AddLine(String.Format("Passing email to Lyris for unsub: {0}", sfAccount.OldValue));
                        }
                    }
                    sqlConn = null;

                    if (iRecsProcessed > 0)
                    {
                        dbi.AddLine(String.Format("Salesforce download successful ({0} records) and EMails Processed ({1} records)", lq.Count, iRecsProcessed));
                    }
                    else
                    {
                        dbi.AddLine(String.Format("Salesforce download successful ({0} records) but none of those records had a valid Old Email Address.", lq.Count));
                    }
                }
                else
                {
                    dbi.AddLine("Salesforce query was processed successfully but no records were returned.");
                }
            }
            catch (Exception ex)
            {
                dbi.AddLine(String.Format("An error occurred: {0}", ex.Message), true);
            }
            dbi.AddLine("-- Ending Import for Unsub Old Emails");

            //Write Log
            ProcessLog(dbi);
        }

        private void DoWelcomeEmailSend()
        {
            string dbconn = "";
#if(DEBUG)
            dbconn = ConfigurationManager.ConnectionStrings["LyrisDev"].ConnectionString;
#else
            dbconn = ConfigurationManager.ConnectionStrings["LyrisLive"].ConnectionString;
#endif
            DoWelcomeEmailSend_GetProductsAndMessages(dbconn);
            DoWelcomeEmailSend_GetSubscribers(dbconn);
        }

        private void DoWelcomeEmailSend_GetSubscribers(string SqlConnection)
        {
            string strFileSaveDateTime = GetLogFileTime();
            DebugInfo dbi = new DebugInfo("LyrisWelcome_GetSubscribers", String.Format(@"C:\temp\LyrisWelcome_GetSubscribers{0}.txt", strFileSaveDateTime));
            BulkData.BulkServer ep = new BulkData.BulkServer(
                SqlConnection,
                String.Format(@"C:\temp\LyrisWelcome_GetSubscribers{0}.csv", strFileSaveDateTime),
                "LyrisWelcomeGetSubscribers",
                BulkData.BulkTableAction.DROP,
                String.Format("SELECT Id, Account__r.Name, Account__r.PersonEmail, Product__c, Product__r.Name FROM Online_Subscription__c WHERE ((Product__r.NU__Entity__c = 'a0Wd0000008yEbZEAU' AND Product__r.Online_Subscription_Type__c = 'Listserv') OR (Product__r.Online_Subscription_Type__c = 'Advisory Group')) AND CreatedDate = LAST_N_DAYS:{0} AND Account__r.PersonEmail != null AND Start_Date__c = LAST_N_DAYS:{0} AND Status__c = 'Active' ORDER BY CreatedDate DESC", NumberOfDays),
                new EnterpriseService()
            );

            dbi.AddLine("LyrisWelcome_GetSubscribers -- Begin");
            bool bQState = true;
            try
            {
                bool bF = false;
                LyrisProductCache subsProducts = new LyrisProductCache(SqlConnection, "SELECT * FROM [Lyris93].[dbo].[LyrisWelcomeGetProductMsgs]");
                for (int i = 1; i <= 3; i++)
                {
                    BulkData.BulkReturn br = BulkData.DownloadBulkDataSalesforceToSQL(
                        ep,
                        new SalesForceClassLibrary.BulkData.BulkSaveAction(
                            delegate(SalesForceClassLibrary.SalesforceEnterprise.sObject o)
                            {
                                string strRetVal = "";
                                SalesForceClassLibrary.SalesforceEnterprise.Online_Subscription__c sfSub = (SalesForceClassLibrary.SalesforceEnterprise.Online_Subscription__c)o;
                                if (sfSub != null)
                                {
                                    strRetVal = ep.QueryHelper.GetAllFieldsAsCSV(sfSub);

                                    if (sfSub.Account__r != null && subsProducts.Products.ContainsKey(sfSub.Product__c))
                                    {
                                        MailMessage mail = new MailMessage();
                                        mail.Body = subsProducts.Products[sfSub.Product__c].WelcomeEmailBody;
                                        mail.IsBodyHtml = true;

                                        bool bSmartWatcher = true;
                                        string strMailTo = sfSub.Account__r.PersonEmail;
                                        string strSubject = "";

#if(DEBUG)
                                        if (strMailTo.ToLower().EndsWith("@leadingage.org"))
                                        {
                                            string[] strM = strMailTo.Split("@".ToCharArray(), 2);
                                            if (strM != null && strM.Length > 0)
                                            {
                                                strMailTo = String.Format("sleadknight+{0}@gmail.com", strM[0]);
                                                bSmartWatcher = false;
                                            }
                                        }

                                        mail.To.Add(new MailAddress("sleadknight@gmail.com"));
                                        strSubject = String.Format("\"{0}\" {1}", strMailTo, subsProducts.Products[sfSub.Product__c].WelcomeEmailSubject);
#else
                                        mail.To.Add(new MailAddress(strMailTo));
                                        strSubject = subsProducts.Products[sfSub.Product__c].WelcomeEmailSubject;
#endif
                                        if (bSmartWatcher && ConfigurationManager.AppSettings["EmailWatcher"] != null)
                                        {
                                            mail.Bcc.Add(new MailAddress(ConfigurationManager.AppSettings["EmailWatcher"].ToString()));
                                        }

                                        try
                                        {
                                            mail.From = new MailAddress(subsProducts.Products[sfSub.Product__c].SubscriptionListEmail);
                                            mail.Subject = strSubject;
                                            mail.SubjectEncoding = Encoding.UTF8;
                                            mail.Priority = MailPriority.Normal;
                                            SmtpClient smtp = new SmtpClient();
                                            if (ConfigurationManager.AppSettings["SmtpHostUser"] != null && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["SmtpHostUser"].ToString()))
                                            {
                                                smtp.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SmtpHostUser"].ToString(), ConfigurationManager.AppSettings["SmtpHostPass"].ToString(), ConfigurationManager.AppSettings["SmtpHostDomain"].ToString());
                                            }
                                            smtp.Host = ConfigurationManager.AppSettings["SmtpHost"].ToString();
                                            smtp.Send(mail);
                                            Random r = new Random();
                                            Thread.Sleep(r.Next(1, 5));
                                        }
                                        catch { }
                                    }

                                }
                                return strRetVal;
                            }
                        )
                    );
                    bQState = br.Success;
                    dbi.AddLine(String.Format("LyrisWelcome_GetSubscribers -- Data Download ({2}) ended with SuccessOf({0}) and Message: {1}", br.Success, br.Message, i));
                    bF = true;
                    if ((br.Message.ToLower().Contains("error in xml")) || (br.Message.ToLower().Contains("operation") && br.Message.ToLower().Contains("time") && br.Message.ToLower().Contains("out")))
                    {
                        bF = false;
                    }
                    if (bF)
                    {
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                dbi.AddLine(String.Format("LyrisWelcome_GetSubscribers -- Data Download failed with fatal error: {0}", ex.Message.ToString()), true);
            }

            //Write Log
            ProcessLog(dbi);
        }

        private void DoWelcomeEmailSend_GetProductsAndMessages(string SqlConnection)
        {
            string strFileSaveDateTime = GetLogFileTime();
            DebugInfo dbi = new DebugInfo("LyrisWelcome_GetProductMsgs", String.Format(@"C:\temp\LyrisWelcome_GetProductMsgs{0}.txt", strFileSaveDateTime));
            BulkData.BulkServer ep = new BulkData.BulkServer(
                SqlConnection,
                String.Format(@"C:\temp\LyrisWelcome_GetProductMsgs{0}.csv", strFileSaveDateTime),
                "LyrisWelcomeGetProductMsgs",
                BulkData.BulkTableAction.DROP,
                "SELECT Id, Name, Lyris_List_Name__c, Welcome_Email_Body__c, Welcome_Email_Subject__c, List_Manager__c, Online_Subscription_Type__c, List_Manager__r.Name, List_Manager__r.PersonEmail, List_Manager__r.FirstName, List_Manager__r.LastName, List_Manager__r.PersonTitle FROM NU__Product__c WHERE ((NU__Entity__c = 'a0Wd0000008yEbZEAU' AND NU__RecordTypeName__c = 'Online Subscription' AND Online_Subscription_Type__c = 'Listserv') OR (Online_Subscription_Type__c = 'Advisory Group'))",
                new EnterpriseService()
            );

            dbi.AddLine("LyrisWelcome_GetProductMsgs -- Begin");
            bool bQState = true;
            try
            {
                bool bF = false;
                for (int i = 1; i <= 3; i++)
                {
                    BulkData.BulkReturn br = BulkData.DownloadBulkDataSalesforceToSQL(
                        ep,
                        new SalesForceClassLibrary.BulkData.BulkSaveAction(
                            delegate(SalesForceClassLibrary.SalesforceEnterprise.sObject o)
                            {
                                string strRetVal = "";
                                SalesForceClassLibrary.SalesforceEnterprise.NU__Product__c sfSubsProd = (SalesForceClassLibrary.SalesforceEnterprise.NU__Product__c)o;
                                if (sfSubsProd != null)
                                {
                                    strRetVal = ep.QueryHelper.GetAllFieldsAsCSV(sfSubsProd);
                                }
                                return strRetVal;
                            }
                        )
                    );
                    bQState = br.Success;
                    dbi.AddLine(String.Format("LyrisWelcome_GetProductMsgs -- Data Download ({2}) ended with SuccessOf({0}) and Message: {1}", br.Success, br.Message, i));
                    bF = true;
                    if ((br.Message.ToLower().Contains("error in xml")) || (br.Message.ToLower().Contains("operation") && br.Message.ToLower().Contains("time") && br.Message.ToLower().Contains("out")))
                    {
                        bF = false;
                    }
                    if (bF)
                    {
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                dbi.AddLine(String.Format("LyrisWelcome_GetProductMsgs -- Data Download failed with fatal error: {0}", ex.Message.ToString()), true);
            }

            //Write Log
            ProcessLog(dbi);
        }

        private void SendTestEmail(string strMailFrom, string strMailTo)
        {
            MailMessage mail = new MailMessage();
            mail.Body = "<p>Test is a test email from Lyris Sync.</p>";
            mail.IsBodyHtml = true;
            mail.To.Add(new MailAddress(strMailTo));
            string strSubject = "Lyris Mail Sync Test Message";
            try
            {
                mail.From = new MailAddress(strMailFrom);
                mail.Subject = strSubject;
                mail.SubjectEncoding = Encoding.UTF8;
                mail.Priority = MailPriority.Normal;
                SmtpClient smtp = new SmtpClient();
                if (ConfigurationManager.AppSettings["SmtpHostUser"] != null && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["SmtpHostUser"].ToString()))
                {
                    smtp.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SmtpHostUser"].ToString(), ConfigurationManager.AppSettings["SmtpHostPass"].ToString(), ConfigurationManager.AppSettings["SmtpHostDomain"].ToString());
                }
                smtp.Host = ConfigurationManager.AppSettings["SmtpHost"].ToString();
                smtp.Send(mail);

                Console.WriteLine(string.Format("Email was sent: Subject: {0}, Body: {1}", mail.Subject, mail.Body));
            }
            catch(Exception ex)
            {
                Console.WriteLine(string.Format("Exception Thrown: {0}", ex.Message));
            }
        }

        private void UpdateEmailMap()
        {
            string strFileSaveDateTime = GetLogFileTime();
            DebugInfo dbi = new DebugInfo("UpdateEmailMap", String.Format(@"C:\temp\UpdateEmailMapQuery{0}.txt", strFileSaveDateTime));
            BulkData.BulkServer ep = new BulkData.BulkServer(
                ConfigurationManager.ConnectionStrings["OldIMIS"].ConnectionString,
                String.Format(@"C:\temp\UpdateEmailMap{0}.csv", strFileSaveDateTime),
                "EmailMap",
                BulkData.BulkTableAction.DROP,
                "SELECT LeadingAge_ID__c, PersonEmail FROM Account WHERE IsPersonAccount = true AND LastModifiedDate = LAST_N_DAYS:1 AND PersonEmail != null",
                new EnterpriseService()
            );

            dbi.AddLine("UpdateEmailMapQuery -- Begin");
            bool bQState = true;
            try
            {
                bool bF = false;
                for (int i = 1; i <= 3; i++)
                {
                    BulkData.BulkReturn br = BulkData.DownloadBulkDataSalesforceToSQL(
                        ep,
                        new SalesForceClassLibrary.BulkData.BulkSaveAction(
                            delegate(SalesForceClassLibrary.SalesforceEnterprise.sObject o)
                            {
                                string strRetVal = "";
                                SalesForceClassLibrary.SalesforceEnterprise.Account sfAccount = (SalesForceClassLibrary.SalesforceEnterprise.Account)o;
                                if (sfAccount != null)
                                {
                                    strRetVal = ep.QueryHelper.GetAllFieldsAsCSV(sfAccount);
                                }
                                return strRetVal;
                            }
                        )
                    );
                    bQState = br.Success;
                    dbi.AddLine(String.Format("UpdateEmailMapQuery -- Data Download ({2}) ended with SuccessOf({0}) and Message: {1}", br.Success, br.Message, i));
                    bF = true;
                    if ((br.Message.ToLower().Contains("error in xml")) || (br.Message.ToLower().Contains("operation") && br.Message.ToLower().Contains("time") && br.Message.ToLower().Contains("out")))
                    {
                        bF = false;
                    }
                    if (bF)
                    {
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                dbi.AddLine(String.Format("UpdateEmailMapQuery -- Data Download failed with fatal error: {0}", ex.Message.ToString()), true);
            }

            //Now run SP to combine temp data into the real table
            if (bQState)
            {
                try
                {
                    SqlConnection sqc = new SqlConnection(ConfigurationManager.ConnectionStrings["OldIMIS"].ConnectionString);
                    SqlCommand sqcc = new SqlCommand("usp_update_email_map", sqc);
                    sqcc.CommandType = CommandType.StoredProcedure;
                    sqcc.Connection.Open();
                    sqcc.ExecuteNonQuery();
                    sqcc.Connection.Close();
                    sqcc = null;

                    dbi.AddLine("UpdateEmailMapQuery -- SQL Cleanup Finished.");
                }
                catch (SqlException sqEx)
                {
                    MessageBox.Show(sqEx.Message);
                    dbi.AddLine(String.Format("UpdateEmailMapQuery -- Error during SQL Cleanup Step: {0}", sqEx.Message), true);
                }
            }

            //Write Log
            ProcessLog(dbi);
        }

        private void DoDownloadForAcademy()
        {
            string strFileSaveDateTime = GetLogFileTime();
#if (DEBUG)
            DebugInfo dbi = new DebugInfo(String.Format(@"\\LeadingAge-db02\c$\temp\ZShashiTempQuery{0}.txt", strFileSaveDateTime));
            BulkData.BulkServer ep = new BulkData.BulkServer(
                ConfigurationManager.ConnectionStrings["LeadershipDev"].ConnectionString,
                String.Format(@"\\LeadingAge-db02\c$\temp\ZShashiTemp{0}.csv", strFileSaveDateTime),
                "Leadership2015",
                BulkData.BulkTableAction.DROP,
                "SELECT Id, LeadingAge_ID__c, FirstName, LastName, PersonEmail, PersonTitle, NU__PrimaryAffiliationRecord__r.NU__IsPrimary__c, NU__PrimaryAffiliationRecord__r.NU__Role__c, NU__PrimaryAffiliationRecord__r.NU__Status__c, NU__PrimaryAffiliation__r.Id, NU__PrimaryAffiliation__r.LeadingAge_ID__c, NU__PrimaryAffiliation__r.Name, NU__PrimaryAffiliation__r.NU__RecordTypeName__c, NU__PrimaryAffiliation__r.BillingStreet, NU__PrimaryAffiliation__r.BillingCity, NU__PrimaryAffiliation__r.BillingState, NU__PrimaryAffiliation__r.BillingPostalCode,  NU__PrimaryAffiliation__r.State_Partner_ID__r.Id, NU__PrimaryAffiliation__r.State_Partner_ID__r.LeadingAge_ID__c, NU__PrimaryAffiliation__r.State_Partner_ID__r.Name, NU__PrimaryAffiliation__r.QF_Covenant_Signed__c,  NU__PrimaryAffiliation__r.CAST_Member__c, NU__PrimaryAffiliation__r.CAST_Member_Product_Name__c, NU__PrimaryAffiliation__r.IAHSA_Membership__r.NU__MembershipProductName__c, NU__PrimaryAffiliation__r.CCRC_Program_Services__c, NU__PrimaryAffiliation__r.AL_Program_Services__c, NU__PrimaryAffiliation__r.HCBS_Program_Services__c, NU__PrimaryAffiliation__r.Housing_Program_Services__c, NU__PrimaryAffiliation__r.Nursing_Program_Services__c, (SELECT Product__r.Name FROM Online_Subscriptions__r WHERE Status__c = 'Active' AND Product__r.Online_Subscription_Type__c = 'Marketing') FROM Account WHERE NU__PrimaryAffiliation__c != NULL AND NU__PrimaryAffiliationRecord__r.NU__Status__c = 'Active' AND NU__PrimaryAffiliation__r.NU__Status__c != 'Inactive' AND PersonEmail != NULL",
                new EnterpriseService()
            );
#else
            DebugInfo dbi = new DebugInfo("Segmentation Data Pull", String.Format(@"C:\temp\ZShashiTempQuery{0}.txt", strFileSaveDateTime));
            BulkData.BulkServer ep = new BulkData.BulkServer(
                ConfigurationManager.ConnectionStrings["LeadershipLive"].ConnectionString,
                String.Format(@"C:\temp\ZShashiTemp{0}.csv", strFileSaveDateTime),
                "Leadership2015",
                BulkData.BulkTableAction.DROP,
                "SELECT Academy_Year__c, LeadingAge_ID__c, Account__r.FirstName, Account__r.LastName, Account__r.PersonEmail, Account__r.NU__PrimaryAffiliation__r.Name, Account__r.Primary_Affiliation_Record_Type_Name__c, Account__r.PersonTitle, Account__r.ShippingStreet, Account__r.ShippingCity, Account__r.ShippingState, Account__r.ShippingPostalCode, Account__r.Phone, Account__r.NU__FullName__c, Account__r.PersonOtherStreet, Account__r.PersonOtherCity, Account__r.PersonOtherState, Account__r.PersonOtherPostalCode, Account__r.PersonHomePhone, Stay_with_employer__c, Financial_Aid__c, Financial_Need__c, Gender__c, Ethnicity__c, Account__r.PersonMobilePhone, Ethnicity_Other__c, Continuum_Other__c, Organization_Uniqueness__c, Position_Other__c, Area_of_Expertise_Other__c, Highest_Education_Level_Other__c, PhD_Other__c, Heard_About__c, Applied_Before__c, Continuum__c, Position_Type__c, Area_of_Expertise__c, Highest_Education_Level__c, Years_of_experience__c, Someone_Completed__c, Organization_Service_Date_1__c, Organization_1__c, Organization_Role_1__c, Organization_Service_Date_2__c, Organization_2__c, Organization_Role_2__c, Organization_Service_Date_3__c, Organization_3__c, Organization_Role_3__c, Organization_Service_Date_4__c, Organization_4__c, Organization_Role_4__c, Organization_Service_Date_5__c, Organization_5__c, Organization_Role_5__c, Activity__c, Essay_1__c, Essay_2__c, Essay_3__c, Submit_Date__c, Submission_Status__c, Memo_Sent__c, Memo_First_Name__c, Memo_Last_Name__c, Memo_Organization__c, Memo_Phone__c, Memo_Email__c, MOU_signed_date__c, MOU_signed_by_applicant__c, MOU_Employer_First_Name__c, MOU_Employer_Last_Name__c, Memo_Job_Title__c, MOU_Employer_Organization__c, MOU_Employer_Phone__c, MOU_Employer_Email__c, MOU_signed_by_employer__c, MOU_Employer_Full_Name__c, Reference_1_Sent__c, Reference_1_First_Name__c, Reference_1_Last_Name__c, Reference_1_Title__c, Reference_1_Organization__c, Reference_1_Link__c, Reference_1_Email__c, Reference_1_Phone__c, Performance_1__c, Reference_1_Received__c, Reference_1_Reason__c, Agreement_1__c, Reference_1_Full_Name__c, Reference_2_Sent__c, Reference_2_First_Name__c, Reference_2_Last_Name__c, Reference_2_Title__c, Reference_2_Organization__c, Reference_2_Link__c, Reference_2_Email__c, Reference_2_Phone__c, Performance_2__c, Reference_2_Received__c, Reference_2_Reason__c, Agreement_2__c, Reference_2_Full_Name__c FROM Academy_Applicant__c WHERE Academy_Year__c = '2015'",
                new EnterpriseService()
            );
#endif
            dbi.AddLine("ZShashiTempQuery -- Begin");
            bool bQState = true;
            try
            {
                bool bF = false;
                for (int i = 1; i <= 3; i++)
                {
                    BulkData.BulkReturn br = BulkData.DownloadBulkDataSalesforceToSQL(
                        ep,
                        new SalesForceClassLibrary.BulkData.BulkSaveAction(
                            delegate(SalesForceClassLibrary.SalesforceEnterprise.sObject o)
                            {
                                string strRetVal = "";
                                SalesForceClassLibrary.SalesforceEnterprise.Academy_Applicant__c sfAccount = (SalesForceClassLibrary.SalesforceEnterprise.Academy_Applicant__c)o;
                                if (sfAccount != null && sfAccount.Account__r != null)
                                {
                                    strRetVal = ep.QueryHelper.GetAllFieldsAsCSV(sfAccount);
                                }
                                return strRetVal;
                            }
                        )
                    );
                    bQState = br.Success;
                    dbi.AddLine(String.Format("ZShashiTempQuery -- Data Download ({2}) ended with SuccessOf({0}) and Message: {1}", br.Success, br.Message, i));
                    bF = true;
                    if ((br.Message.ToLower().Contains("error in xml")) || (br.Message.ToLower().Contains("operation") && br.Message.ToLower().Contains("time") && br.Message.ToLower().Contains("out")))
                    {
                        bF = false;
                    }
                    if (bF)
                    {
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                dbi.AddLine(String.Format("ZShashiTempQuery -- Data Download failed with fatal error: {0}", ex.Message.ToString()), true);
            }

            //Write Log
            ProcessLog(dbi);
        }

        private void DoDownloadForShashiTemp()
        {
            string strFileSaveDateTime = GetLogFileTime();
#if (DEBUG)
            DebugInfo dbi = new DebugInfo(String.Format(@"\\LeadingAge-db02\c$\temp\ZShashiTempQuery{0}.txt", strFileSaveDateTime));
            BulkData.BulkServer ep = new BulkData.BulkServer(
                ConfigurationManager.ConnectionStrings["LyrisDev"].ConnectionString,
                String.Format(@"\\LeadingAge-db02\c$\temp\ZShashiTemp{0}.csv", strFileSaveDateTime),
                "ZShashiTemp",
                BulkData.BulkTableAction.DROP,
                "SELECT Id, LeadingAge_ID__c, FirstName, LastName, PersonEmail, PersonTitle, NU__PrimaryAffiliationRecord__r.NU__IsPrimary__c, NU__PrimaryAffiliationRecord__r.NU__Role__c, NU__PrimaryAffiliationRecord__r.NU__Status__c, NU__PrimaryAffiliation__r.Id, NU__PrimaryAffiliation__r.LeadingAge_ID__c, NU__PrimaryAffiliation__r.Name, NU__PrimaryAffiliation__r.NU__RecordTypeName__c, NU__PrimaryAffiliation__r.BillingStreet, NU__PrimaryAffiliation__r.BillingCity, NU__PrimaryAffiliation__r.BillingState, NU__PrimaryAffiliation__r.BillingPostalCode,  NU__PrimaryAffiliation__r.State_Partner_ID__r.Id, NU__PrimaryAffiliation__r.State_Partner_ID__r.LeadingAge_ID__c, NU__PrimaryAffiliation__r.State_Partner_ID__r.Name, NU__PrimaryAffiliation__r.QF_Covenant_Signed__c,  NU__PrimaryAffiliation__r.CAST_Member__c, NU__PrimaryAffiliation__r.CAST_Member_Product_Name__c, NU__PrimaryAffiliation__r.IAHSA_Membership__r.NU__MembershipProductName__c, NU__PrimaryAffiliation__r.CCRC_Program_Services__c, NU__PrimaryAffiliation__r.AL_Program_Services__c, NU__PrimaryAffiliation__r.HCBS_Program_Services__c, NU__PrimaryAffiliation__r.Housing_Program_Services__c, NU__PrimaryAffiliation__r.Nursing_Program_Services__c, (SELECT Product__r.Name FROM Online_Subscriptions__r WHERE Status__c = 'Active' AND Product__r.Online_Subscription_Type__c = 'Marketing') FROM Account WHERE NU__PrimaryAffiliation__c != NULL AND NU__PrimaryAffiliationRecord__r.NU__Status__c = 'Active' AND NU__PrimaryAffiliation__r.NU__Status__c != 'Inactive' AND PersonEmail != NULL",
                new EnterpriseService()
            );
#else
            DebugInfo dbi = new DebugInfo("Segmentation Data Pull", String.Format(@"C:\temp\ZShashiTempQuery{0}.txt", strFileSaveDateTime));
            BulkData.BulkServer ep = new BulkData.BulkServer(
                ConfigurationManager.ConnectionStrings["LyrisLive"].ConnectionString,
                String.Format(@"C:\temp\ZShashiTemp{0}.csv", strFileSaveDateTime),
                "ZShashiTemp",
                BulkData.BulkTableAction.DROP,
                "SELECT Id, LeadingAge_ID__c, Name, FirstName, LastName, PersonEmail, PersonTitle, Phone, PersonOtherPhone, PersonHomePhone, PersonMobilePhone,  Operational_Level__pc, Organizational_Function_Expertise__pc, NU__PrimaryAffiliation__r.LeadingAge_Member_Company__c, NU__PrimaryAffiliation__r.Name, NU__PrimaryAffiliation__r.BillingStreet, NU__PrimaryAffiliation__r.BillingCity, NU__PrimaryAffiliation__r.BillingState, NU__PrimaryAffiliation__r.BillingPostalCode, NU__PrimaryAffiliation__r.BillingCountry,    (SELECT NU__Event__r.NU__ShortName__c FROM NU__Registrations2__r)  FROM Account WHERE Operational_Level__pc IN( 'CEO/President','CFO','CIO','COO','CSO/CMO') AND NU__PrimaryAffiliation__c != NULL AND NU__PrimaryAffiliationRecord__r.NU__Status__c = 'Active' AND NU__PrimaryAffiliation__r.NU__Status__c != 'Inactive' AND (NU__PrimaryAffiliation__r.NU__RecordTypeName__c = 'Multi-site Organization') AND (NU__PrimaryAffiliation__r.LeadingAge_Member_Company__c = 'Member' OR NU__PrimaryAffiliation__r.LeadingAge_Member_Company__c = 'member')",
                new EnterpriseService()
            );
#endif
            dbi.AddLine("ZShashiTempQuery -- Begin");
            bool bQState = true;
            try
            {
                bool bF = false;
                for (int i = 1; i <= 3; i++)
                {
                    BulkData.BulkReturn br = BulkData.DownloadBulkDataSalesforceToSQL(
                        ep,
                        new SalesForceClassLibrary.BulkData.BulkSaveAction(
                            delegate(SalesForceClassLibrary.SalesforceEnterprise.sObject o)
                            {
                                string strRetVal = "";
                                SalesForceClassLibrary.SalesforceEnterprise.Account sfAccount = (SalesForceClassLibrary.SalesforceEnterprise.Account)o;
                                if (sfAccount != null && sfAccount.NU__PrimaryAffiliation__r != null)
                                {
                                    strRetVal = ep.QueryHelper.GetAllFieldsAsCSV(sfAccount);
                                }
                                return strRetVal;
                            }
                        )
                    );
                    bQState = br.Success;
                    dbi.AddLine(String.Format("ZShashiTempQuery -- Data Download ({2}) ended with SuccessOf({0}) and Message: {1}", br.Success, br.Message, i));
                    bF = true;
                    if ((br.Message.ToLower().Contains("error in xml")) || (br.Message.ToLower().Contains("operation") && br.Message.ToLower().Contains("time") && br.Message.ToLower().Contains("out")))
                    {
                        bF = false;
                    }
                    if (bF)
                    {
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                dbi.AddLine(String.Format("ZShashiTempQuery -- Data Download failed with fatal error: {0}", ex.Message.ToString()), true);
            }

            //Write Log
            ProcessLog(dbi);
        }

        private void DoLyrisCEMOSegment()
        {
            string strFileSaveDateTime = GetLogFileTime();
            string strDbConn = "";
            string strDbFilePath = "";
#if (DEBUG)
            strDbConn = ConfigurationManager.ConnectionStrings["LyrisDev"].ConnectionString;
            strDbFilePath = @"\\LeadingAge-db02\c$\temp";
#else
            strDbConn = ConfigurationManager.ConnectionStrings["LyrisLive"].ConnectionString;
            strDbFilePath = @"C:\temp";
#endif
            DebugInfo dbi = new DebugInfo("Segmentation Data Pull", String.Format(@"{0}\DoLyrisCEMOSegment{1}.txt", strDbFilePath, strFileSaveDateTime));
            BulkData.BulkServer ep = new BulkData.BulkServer(
                strDbConn,
                String.Format(@"{0}\LyrisNimbleCEMOSegmentsImport{1}.csv", strDbFilePath, strFileSaveDateTime),
                "SegmentCEMO",
                BulkData.BulkTableAction.DROP,
                "SELECT Id, LeadingAge_ID__c, FirstName, LastName, PersonEmail, PersonTitle, Operational_Level__pc, Organizational_Function_Expertise__pc, NU__PrimaryAffiliationRecord__r.NU__Role__c, NU__PrimaryAffiliation__r.Id, NU__PrimaryAffiliation__r.LeadingAge_ID__c, NU__PrimaryAffiliation__r.Name, NU__PrimaryAffiliation__r.NU__RecordTypeName__c FROM Account WHERE PersonEmail != NULL AND CEMO__pc = true",
                new EnterpriseService()
            );

            dbi.AddLine("DoLyrisCEMOSegment -- Begin");
            bool bQState = true;
            try
            {
                bool bF = false;
                for (int i = 1; i <= 3; i++)
                {
                    BulkData.BulkReturn br = BulkData.DownloadBulkDataSalesforceToSQL(
                        ep,
                        new SalesForceClassLibrary.BulkData.BulkSaveAction(
                            delegate(SalesForceClassLibrary.SalesforceEnterprise.sObject o)
                            {
                                string strRetVal = "";
                                SalesForceClassLibrary.SalesforceEnterprise.Account sfAccount = (SalesForceClassLibrary.SalesforceEnterprise.Account)o;
                                if (sfAccount != null)
                                {
                                    strRetVal = ep.QueryHelper.GetAllFieldsAsCSV(sfAccount);
                                }
                                return strRetVal;
                            }
                        )
                    );
                    bQState = br.Success;
                    dbi.AddLine(String.Format("DoLyrisCEMOSegment -- Data Download ({2}) ended with SuccessOf({0}) and Message: {1}", br.Success, br.Message, i));
                    bF = true;
                    if ((br.Message.ToLower().Contains("error in xml")) || (br.Message.ToLower().Contains("operation") && br.Message.ToLower().Contains("time") && br.Message.ToLower().Contains("out")))
                    {
                        bF = false;
                    }
                    if (bF)
                    {
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                dbi.AddLine(String.Format("DoLyrisCEMOSegment -- Data Download failed with fatal error: {0}", ex.Message.ToString()), true);
            }

            if (bQState)
            {
                try
                {
                    string strExecSQL = "";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[SegmentCEMO].Account@LeadingAge_ID__c', 'ind_id', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[SegmentCEMO].Account@FirstName', 'ind_first', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[SegmentCEMO].Account@LastName', 'ind_last', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[SegmentCEMO].Account@PersonEmail', 'ind_email', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[SegmentCEMO].Account@PersonTitle', 'ind_title', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[SegmentCEMO].Account@Operational_Level__pc', 'oper_level', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[SegmentCEMO].Account@Organizational_Function_Expertise__pc', 'org_func_exper', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[SegmentCEMO].Account@NU__PrimaryAffiliationRecord__r@NU__Role__c', 'relationship_type', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[SegmentCEMO].Account@NU__PrimaryAffiliation__r@LeadingAge_ID__c', 'company_id', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[SegmentCEMO].Account@NU__PrimaryAffiliation__r@Name', 'company_name', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[SegmentCEMO].Account@NU__PrimaryAffiliation__r@NU__RecordTypeName__c', 'company_type', 'COLUMN'\r\n";
#if (DEBUG)
            SqlConnection sqc = new SqlConnection(ConfigurationManager.ConnectionStrings["LyrisDev"].ConnectionString);
#else
                SqlConnection sqc = new SqlConnection(ConfigurationManager.ConnectionStrings["LyrisLive"].ConnectionString);
#endif
                    SqlCommand sqcc = new SqlCommand(strExecSQL, sqc);
                    sqcc.Connection.Open();
                    sqcc.ExecuteNonQuery();
                    sqcc.Connection.Close();
                    sqcc = null;

                    dbi.AddLine("DoLyrisCEMOSegment -- SQL Cleanup Finished.");
                }
                catch (SqlException sqEx)
                {
                    MessageBox.Show(sqEx.Message);
                    dbi.AddLine(String.Format("DoLyrisCEMOSegment -- Error during SQL Cleanup Step: {0}", sqEx.Message), true);
                }
            }

            //Write Log
            ProcessLog(dbi);
        }

        private void DoLyrisSegmentsMaster()
        {
            string strFileSaveDateTime = GetLogFileTime();
#if (DEBUG)
            DebugInfo dbi = new DebugInfo(String.Format(@"\\LeadingAge-db02\c$\temp\DoLyrisSegmentsMaster{0}.txt", strFileSaveDateTime));
            BulkData.BulkServer ep = new BulkData.BulkServer(
                ConfigurationManager.ConnectionStrings["LyrisDev"].ConnectionString,
                String.Format(@"\\LeadingAge-db02\c$\temp\LyrisNimbleBaseMasterSegmentsImport{0}.csv", strFileSaveDateTime),
                "LyrisNimbleBaseMasterSegmentsImport",
                BulkData.BulkTableAction.DROP,
                "SELECT Id, LeadingAge_ID__c, FirstName, LastName, PersonEmail, PersonTitle, NU__PrimaryAffiliationRecord__r.NU__IsPrimary__c, NU__PrimaryAffiliationRecord__r.NU__Role__c, NU__PrimaryAffiliationRecord__r.NU__Status__c, NU__PrimaryAffiliation__r.Id, NU__PrimaryAffiliation__r.LeadingAge_ID__c, NU__PrimaryAffiliation__r.Name, NU__PrimaryAffiliation__r.NU__RecordTypeName__c, NU__PrimaryAffiliation__r.BillingStreet, NU__PrimaryAffiliation__r.BillingCity, NU__PrimaryAffiliation__r.BillingState, NU__PrimaryAffiliation__r.BillingPostalCode,  NU__PrimaryAffiliation__r.State_Partner_ID__r.Id, NU__PrimaryAffiliation__r.State_Partner_ID__r.LeadingAge_ID__c, NU__PrimaryAffiliation__r.State_Partner_ID__r.Name, NU__PrimaryAffiliation__r.QF_Covenant_Signed__c,  NU__PrimaryAffiliation__r.CAST_Member__c, NU__PrimaryAffiliation__r.CAST_Member_Product_Name__c, NU__PrimaryAffiliation__r.IAHSA_Membership__r.NU__MembershipProductName__c, NU__PrimaryAffiliation__r.CCRC_Program_Services__c, NU__PrimaryAffiliation__r.AL_Program_Services__c, NU__PrimaryAffiliation__r.HCBS_Program_Services__c, NU__PrimaryAffiliation__r.Housing_Program_Services__c, NU__PrimaryAffiliation__r.Nursing_Program_Services__c, (SELECT Product__r.Name FROM Online_Subscriptions__r WHERE Status__c = 'Active' AND Product__r.Online_Subscription_Type__c = 'Marketing') FROM Account WHERE NU__PrimaryAffiliation__c != NULL AND NU__PrimaryAffiliationRecord__r.NU__Status__c = 'Active' AND NU__PrimaryAffiliation__r.NU__Status__c != 'Inactive' AND PersonEmail != NULL",
                new EnterpriseService()
            );
#else
            DebugInfo dbi = new DebugInfo("Segmentation Data Pull", String.Format(@"C:\temp\DoLyrisSegmentsMaster{0}.txt", strFileSaveDateTime));
            BulkData.BulkServer ep = new BulkData.BulkServer(
                ConfigurationManager.ConnectionStrings["LyrisLive"].ConnectionString,
                String.Format(@"C:\temp\LyrisNimbleBaseMasterSegmentsImport{0}.csv", strFileSaveDateTime),
                "LyrisNimbleBaseMasterSegmentsImport",
                BulkData.BulkTableAction.DROP,
                "SELECT LeadingAge_ID__c, FirstName, LastName, PersonEmail, PersonTitle, SystemModstamp, NU__Membership__r.MembershipTypeProductName__c, NU__MemberThru__c, NU__PrimaryAffiliationRecord__r.NU__Role__c, NU__PrimaryAffiliationRecord__r.NU__Status__c, NU__PrimaryAffiliation__r.Exhibitor_Product_Lines__c, NU__PrimaryAffiliation__r.LeadingAge_ID__c, NU__PrimaryAffiliation__r.Name, NU__PrimaryAffiliation__r.NU__RecordTypeName__c, NU__PrimaryAffiliation__r.Type, NU__PrimaryAffiliation__r.BillingStreet, NU__PrimaryAffiliation__r.BillingCity, NU__PrimaryAffiliation__r.BillingState, NU__PrimaryAffiliation__r.BillingPostalCode, NU__PrimaryAffiliation__r.BillingCountry, NU__PrimaryAffiliation__r.State_Partner_ID__r.LeadingAge_ID__c, NU__PrimaryAffiliation__r.State_Partner_ID__r.Name, NU__PrimaryAffiliation__r.QF_Covenant_Signed__c,  NU__PrimaryAffiliation__r.CAST_Member__c, NU__PrimaryAffiliation__r.CAST_Member_Product_Name__c, NU__PrimaryAffiliation__r.IAHSA_Membership__r.NU__MembershipProductName__c, NU__PrimaryAffiliation__r.CCRC_Program_Services__c, NU__PrimaryAffiliation__r.AL_Program_Services__c, NU__PrimaryAffiliation__r.HCBS_Program_Services__c, NU__PrimaryAffiliation__r.Housing_Program_Services__c, NU__PrimaryAffiliation__r.Nursing_Program_Services__c, (SELECT Product__r.Name FROM Online_Subscriptions__r WHERE Status__c = 'Active' AND Product__r.Online_Subscription_Type__c = 'Marketing'), (SELECT Session__r.Conference__r.NU__ShortName__c FROM Session_Speakers__r), (SELECT NU__Event__r.NU__ShortName__c FROM NU__Registrations2__r), (SELECT Event__r.NU__ShortName__c FROM Exhibitors__r), (SELECT NU__Committee__r.Name FROM NU__CommitteeMemberships__r WHERE NU__StatusFlag__c LIKE '%current.png%'),Operational_Level__pc,Organizational_Function_Expertise__pc FROM Account WHERE NU__PrimaryAffiliation__c != NULL AND NU__PrimaryAffiliationRecord__r.NU__Status__c = 'Active' AND NU__PrimaryAffiliation__r.NU__Status__c != 'Inactive' AND PersonEmail != NULL AND (NU__PrimaryAffiliation__r.NU__RecordTypeName__c = 'Multi-site Organization' OR NU__PrimaryAffiliation__r.NU__RecordTypeName__c = 'Company' OR NU__PrimaryAffiliation__r.NU__RecordTypeName__c = 'Provider' OR NU__PrimaryAffiliation__r.NU__RecordTypeName__c = 'Corporate Alliance/Sponsor') AND Id IN (SELECT Account__c FROM Online_Subscription__c)",
                new EnterpriseService()
            );
#endif
            dbi.AddLine("DoLyrisSegmentsMaster -- Begin");
            bool bQState = true;
            try
            {
                bool bF = false;
                for (int i = 1; i <= 3; i++)
                {
                    BulkData.BulkReturn br = BulkData.DownloadBulkDataSalesforceToSQL(
                        ep,
                        new SalesForceClassLibrary.BulkData.BulkSaveAction(
                            delegate(SalesForceClassLibrary.SalesforceEnterprise.sObject o)
                            {
                                string strRetVal = "";
                                SalesForceClassLibrary.SalesforceEnterprise.Account sfAccount = (SalesForceClassLibrary.SalesforceEnterprise.Account)o;
                                if (sfAccount != null && sfAccount.NU__PrimaryAffiliation__r != null)
                                {
                                    strRetVal = ep.QueryHelper.GetAllFieldsAsCSV(sfAccount);
                                }
                                return strRetVal;
                            }
                        )
                    );
                    bQState = br.Success;
                    dbi.AddLine(String.Format("DoLyrisSegmentsMaster -- Data Download ({2}) ended with SuccessOf({0}) and Message: {1}", br.Success, br.Message, i), true);
                    bF = true;
                    if ((br.Message.ToLower().Contains("error in xml")) || (br.Message.ToLower().Contains("operation") && br.Message.ToLower().Contains("time") && br.Message.ToLower().Contains("out")))
                    {
                        bF = false;
                    }
                    if (bF)
                    {
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                dbi.AddLine(String.Format("DoLyrisSegmentsMaster -- Data Download failed with fatal error: {0}", ex.Message.ToString()), true);
            }

            if (bQState)
            {
                try
                {
                    string strExecSQL = "";
                    strExecSQL = "ALTER TABLE dbo.LyrisNimbleBaseMasterSegmentsImport ADD company_address NVARCHAR(500) NULL\r\n";
                    SqlConnection sqc = new SqlConnection(ConfigurationManager.ConnectionStrings["LyrisLive"].ConnectionString);
                    SqlCommand sqcc = new SqlCommand(strExecSQL, sqc);
                    sqcc.Connection.Open();
                    sqcc.ExecuteNonQuery();
                    sqcc.Connection.Close();

                    //strExecSQL += "GO\r\n";
                    strExecSQL = "UPDATE dbo.LyrisNimbleBaseMasterSegmentsImport SET company_address = RTRIM(Account@NU__PrimaryAffiliation__r@BillingStreet) + ',' + RTRIM(Account@NU__PrimaryAffiliation__r@BillingCity) + ',' + RTRIM(Account@NU__PrimaryAffiliation__r@BillingState) + ',' + RTRIM(Account@NU__PrimaryAffiliation__r@BillingPostalCode)\r\n";
                    strExecSQL += "ALTER TABLE dbo.LyrisNimbleBaseMasterSegmentsImport DROP COLUMN Account@NU__PrimaryAffiliation__r@BillingStreet\r\n";
                    strExecSQL += "ALTER TABLE dbo.LyrisNimbleBaseMasterSegmentsImport DROP COLUMN Account@NU__PrimaryAffiliation__r@BillingCity\r\n";
                    strExecSQL += "ALTER TABLE dbo.LyrisNimbleBaseMasterSegmentsImport DROP COLUMN Account@NU__PrimaryAffiliation__r@BillingPostalCode\r\n";
                    strExecSQL += "ALTER TABLE dbo.LyrisNimbleBaseMasterSegmentsImport ADD AccountName NVARCHAR(255) NULL\r\n";

                    sqcc.CommandText = strExecSQL;
                    sqcc.Connection.Open();
                    sqcc.ExecuteNonQuery();
                    sqcc.Connection.Close();

                    //strExecSQL += "GO\r\n";
                    strExecSQL = "UPDATE dbo.LyrisNimbleBaseMasterSegmentsImport SET AccountName = Account@PersonEmail\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[LyrisNimbleBaseMasterSegmentsImport].Account@LeadingAge_ID__c', 'ind_id', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[LyrisNimbleBaseMasterSegmentsImport].Account@FirstName', 'ind_first', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[LyrisNimbleBaseMasterSegmentsImport].Account@LastName', 'ind_last', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[LyrisNimbleBaseMasterSegmentsImport].Account@PersonEmail', 'ind_email', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[LyrisNimbleBaseMasterSegmentsImport].Account@PersonTitle', 'ind_title', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[LyrisNimbleBaseMasterSegmentsImport].Account@SystemModstamp', 'RecentDate', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[LyrisNimbleBaseMasterSegmentsImport].Account@Operational_Level__pc', 'oper_level', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[LyrisNimbleBaseMasterSegmentsImport].Account@Organizational_Function_Expertise__pc', 'org_func_exper', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[LyrisNimbleBaseMasterSegmentsImport].Account@NU__PrimaryAffiliationRecord__r@NU__Role__c', 'relationship_type', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[LyrisNimbleBaseMasterSegmentsImport].Account@NU__PrimaryAffiliationRecord__r@NU__Status__c', 'company_status', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[LyrisNimbleBaseMasterSegmentsImport].Account@NU__PrimaryAffiliation__r@Exhibitor_Product_Lines__c', 'service_type', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[LyrisNimbleBaseMasterSegmentsImport].Account@NU__PrimaryAffiliation__r@LeadingAge_ID__c', 'company_id', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[LyrisNimbleBaseMasterSegmentsImport].Account@NU__PrimaryAffiliation__r@Name', 'company_name', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[LyrisNimbleBaseMasterSegmentsImport].Account@NU__PrimaryAffiliation__r@NU__RecordTypeName__c', 'company_type', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[LyrisNimbleBaseMasterSegmentsImport].Account@NU__PrimaryAffiliation__r@Type', 'company_subtype', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[LyrisNimbleBaseMasterSegmentsImport].Account@NU__PrimaryAffiliation__r@BillingState', 'States', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[LyrisNimbleBaseMasterSegmentsImport].Account@NU__PrimaryAffiliation__r@BillingCountry', 'country', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[LyrisNimbleBaseMasterSegmentsImport].Account@NU__PrimaryAffiliation__r@State_Partner_ID__r@LeadingAge_ID__c', 'stateID', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[LyrisNimbleBaseMasterSegmentsImport].Account@NU__PrimaryAffiliation__r@State_Partner_ID__r@Name', 'state_association', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[LyrisNimbleBaseMasterSegmentsImport].Account@NU__PrimaryAffiliation__r@QF_Covenant_Signed__c', 'quality_first', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[LyrisNimbleBaseMasterSegmentsImport].Account@NU__PrimaryAffiliation__r@CAST_Member__c', 'cast_membership', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[LyrisNimbleBaseMasterSegmentsImport].Account@NU__PrimaryAffiliation__r@CAST_Member_Product_Name__c', 'cast_membercategory', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[LyrisNimbleBaseMasterSegmentsImport].Account@NU__PrimaryAffiliation__r@IAHSA_Membership__r@NU__MembershipProductName__c', 'IAHSA', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[LyrisNimbleBaseMasterSegmentsImport].Account@NU__PrimaryAffiliation__r@CCRC_Program_Services__c', 'CCRC', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[LyrisNimbleBaseMasterSegmentsImport].Account@NU__PrimaryAffiliation__r@AL_Program_Services__c', 'AL', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[LyrisNimbleBaseMasterSegmentsImport].Account@NU__PrimaryAffiliation__r@HCBS_Program_Services__c', 'HCBS', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[LyrisNimbleBaseMasterSegmentsImport].Account@NU__PrimaryAffiliation__r@Housing_Program_Services__c', 'SH', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[LyrisNimbleBaseMasterSegmentsImport].Account@NU__PrimaryAffiliation__r@Nursing_Program_Services__c', 'NC', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[LyrisNimbleBaseMasterSegmentsImport].Online_Subscriptions__r@Product__r@Name', 'interest_area', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[LyrisNimbleBaseMasterSegmentsImport].Session_Speakers__r@Session__r@Conference__r@NU__ShortName__c', 'speakers', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[LyrisNimbleBaseMasterSegmentsImport].NU__Registrations2__r@NU__Event__r@NU__ShortName__c', 'conference', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[LyrisNimbleBaseMasterSegmentsImport].Exhibitors__r@Event__r@NU__ShortName__c', 'Exhibitor', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[LyrisNimbleBaseMasterSegmentsImport].NU__CommitteeMemberships__r@NU__Committee__r@Name', 'CommitteeCode', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[LyrisNimbleBaseMasterSegmentsImport].Account@NU__Membership__r@MembershipTypeProductName__c', 'company_category', 'COLUMN'\r\n";
                    strExecSQL += "EXEC sp_rename '[Lyris93].[dbo].[LyrisNimbleBaseMasterSegmentsImport].Account@NU__MemberThru__c', 'Paid_Thru', 'COLUMN'\r\n";
                    strExecSQL += "ALTER TABLE dbo.LyrisNimbleBaseMasterSegmentsImport ADD FunctionalTitle NVARCHAR(1) NULL\r\n";
                    strExecSQL += "ALTER TABLE dbo.LyrisNimbleBaseMasterSegmentsImport ADD RelType NVARCHAR(1) NULL\r\n";

                    sqcc.CommandText = strExecSQL;
                    sqcc.Connection.Open();
                    sqcc.ExecuteNonQuery();
                    sqcc.Connection.Close();

                    //strExecSQL += "GO\r\n";
                    strExecSQL = "IF ((SELECT COUNT(*) FROM dbo.LyrisNimbleBaseMasterSegmentsImport WHERE company_type = 'Multi-site Organization' OR company_type = 'Provider' OR company_type = 'Company') > 0)\r\n";
                    strExecSQL += "BEGIN\r\n";
                    strExecSQL += "IF EXISTS(SELECT [name] FROM dbo.sysobjects WHERE [name] = 'Provider_table') DROP TABLE dbo.Provider_table\r\n";
                    strExecSQL += "SELECT ISNULL(ind_id, '') AS ind_id, ISNULL(ind_first, '') AS ind_first, ISNULL(ind_last, '') AS ind_last, ISNULL(ind_title, '') AS ind_title, ISNULL(ind_email, '') AS ind_email, ISNULL(AccountName, '') AS AccountName, ISNULL(FunctionalTitle, '') AS FunctionalTitle, ISNULL(relationship_type, '') AS relationship_type, ISNULL(company_id, '') AS company_id, ISNULL(RelType, '') AS RelType, ISNULL(company_type, '') AS company_type, ISNULL(company_subtype, '') AS company_subtype, ISNULL(company_status, '') AS company_status, ISNULL(company_name, '') AS company_name, ISNULL(company_address, '') AS company_address, ISNULL(States, '') AS States, ISNULL(country, '') AS country, ISNULL(stateID, '') AS stateID, ISNULL(state_association, '') AS state_association, ISNULL(quality_first, '') AS quality_first, ISNULL(speakers, '') AS speakers, ISNULL(conference, '') AS conference, ISNULL(interest_area, '') AS interest_area, ISNULL(Exhibitor, '') AS Exhibitor, ISNULL(CommitteeCode, '') AS CommitteeCode, ISNULL(RecentDate, '') AS RecentDate, ISNULL(cast_membership, '') AS cast_membership, ISNULL(cast_membercategory, '') AS cast_membercategory, ISNULL(IAHSA, '') AS IAHSA, ISNULL(CCRC, '') AS CCRC, ISNULL(AL, '') AS AL, ISNULL(HCBS, '') AS HCBS, ISNULL(SH, '') AS SH, ISNULL(NC, '') AS NC, ISNULL(oper_level, '') AS oper_level, ISNULL(org_func_exper, '') AS org_func_exper\r\n";
                    strExecSQL += "INTO dbo.Provider_table\r\n";
                    strExecSQL += "FROM dbo.LyrisNimbleBaseMasterSegmentsImport\r\n";
                    strExecSQL += "WHERE company_type = 'Multi-site Organization' OR company_type = 'Provider' OR company_type = 'Company'\r\n";
                    strExecSQL += "CREATE CLUSTERED INDEX [email] ON [Lyris93].[dbo].[Provider_table] ([ind_email] ASC)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]";
                    strExecSQL += "END\r\n";
                    strExecSQL += "IF ((SELECT COUNT(*) FROM dbo.LyrisNimbleBaseMasterSegmentsImport WHERE NOT company_type = 'Multi-site Organization' AND NOT company_type = 'Provider' AND NOT company_type = 'Company') > 0)\r\n";
                    strExecSQL += "BEGIN\r\n";
                    strExecSQL += "IF EXISTS(SELECT [name] FROM dbo.sysobjects WHERE [name] = 'Business_Exhibitor_table') DROP TABLE dbo.Business_Exhibitor_table\r\n";
                    strExecSQL += "SELECT ISNULL(ind_id, '') AS ind_id, ISNULL(ind_first, '') AS ind_first, ISNULL(ind_last, '') AS ind_last, ISNULL(ind_title, '') AS ind_title, ISNULL(ind_email, '') AS ind_email, ISNULL(AccountName, '') AS AccountName, ISNULL(relationship_type, '') AS relationship_type, ISNULL(company_id, '') AS company_id, ISNULL(company_type, '') AS company_type, ISNULL(company_subtype, '') AS company_subtype, ISNULL(company_status, '') AS company_status, ISNULL(company_name, '') AS company_name, ISNULL(company_address, '') AS company_address, ISNULL(company_category, '') AS company_category, ISNULL(country, '') AS country, ISNULL(service_type, '') AS service_type, ISNULL(speakers, '') AS speakers, ISNULL(conference, '') AS conference, ISNULL(interest_area, '') AS interest_area, ISNULL(Exhibitor, '') AS Exhibitor, ISNULL(Paid_Thru, '') AS Paid_Thru, ISNULL(oper_level, '') AS oper_level, ISNULL(org_func_exper, '') AS org_func_exper\r\n";
                    strExecSQL += "INTO Business_Exhibitor_table\r\n";
                    strExecSQL += "FROM dbo.LyrisNimbleBaseMasterSegmentsImport\r\n";
                    strExecSQL += "WHERE NOT company_type = 'Multi-site Organization' AND NOT company_type = 'Provider' AND NOT company_type = 'Company'\r\n";
                    strExecSQL += "CREATE CLUSTERED INDEX [email] ON [Lyris93].[dbo].[Business_Exhibitor_table] ([ind_email] ASC)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]";
                    strExecSQL += "END\r\n";

                    sqcc.CommandText = strExecSQL;
                    sqcc.Connection.Open();
                    sqcc.ExecuteNonQuery();
                    sqcc.Connection.Close();
                    sqcc = null;

                    dbi.AddLine("DoLyrisSegmentsMaster -- SQL Cleanup Finished.");
                }
                catch (SqlException sqEx)
                {
                    MessageBox.Show(sqEx.Message);
                    dbi.AddLine(String.Format("DoLyrisSegmentsMaster -- Error during SQL Cleanup Step: {0}", sqEx.Message), true);
                }
            }
            else
            {
                dbi.AddLine("DoLyrisSegmentsMaster -- SQL step did nothing because the bulk download did not succeed.", true);
            }

            //Write Log
            ProcessLog(dbi);
        }

        //Original Code
        private void btnNewGo_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Click [Yes] to perform main subscription sync, click [No] to perform email history sync, click [cancel] to do nothing", "Choose:", MessageBoxButtons.YesNoCancel);
            if (dr == System.Windows.Forms.DialogResult.Yes)
            {
                DoLyrisSubscriptionSync(SyncTypes.OnlineSubscriptions);
            }
            else if (dr == System.Windows.Forms.DialogResult.No)
            {
                DoLyrisSubscriptionSync(SyncTypes.EmailHistory);
            }
        }

        private void btnSendHeld_Click(object sender, EventArgs e)
        {
            DoLyrisSubscriptionSendHeld();
        }

        private void btnSegments_Click(object sender, EventArgs e)
        {
            DoLyrisSegmentsMaster();
        }

        private void btnWelcome_Click(object sender, EventArgs e)
        {
            DoWelcomeEmailSend();
        }
    }

    internal class DebugInfo
    {
        public class DebugLine
        {
            public string Line { get; set; }
            public bool IsFatal { get; set; }

            public DebugLine()
            {
                this.Line = "";
                this.IsFatal = false;
            }

            public DebugLine(string Line)
            {
                this.Line = Line;
                this.IsFatal = false;
            }

            public DebugLine(string Line, bool IsFatal)
            {
                this.Line = Line;
                this.IsFatal = IsFatal;
            }
        }

        public string DebugSubject { get; set; }
        private List<DebugLine> _lines;
        private int _linec = 0;
        private string _fn = "";

        public DebugInfo()
        {
            this.DebugSubject = "";
            this._lines = new List<DebugLine>();
        }

        public DebugInfo(string LogFilePath)
        {
            this.DebugSubject = "";
            this._lines = new List<DebugLine>();
            this._fn = LogFilePath;
        }

        public DebugInfo(string Subject, string LogFilePath)
        {
            this.DebugSubject = Subject;
            this._lines = new List<DebugLine>();
            this._fn = LogFilePath;
        }

        public void Clear()
        {
            _lines.Clear();
            _linec = 0;
            _fn = "";
        }

        public void Clear(string LogFilePath)
        {
            _lines.Clear();
            _linec = 0;
            _fn = LogFilePath;
        }

        public void AddLine(string Line)
        {
            _linec++;
            _lines.Add(new DebugLine(_linec.ToString().PadLeft(5, '0') + ": " + Line + "\r\n"));
        }

        public void AddLine(string Line, bool IsFatal)
        {
            _linec++;
            _lines.Add(new DebugLine(_linec.ToString().PadLeft(5, '0') + ": " + Line + "\r\n", IsFatal));
        }

        public string GetLines()
        {
            string strRetVal = "";
            foreach (DebugLine strItem in _lines)
            {
                strRetVal += strItem.Line;
            }
            return strRetVal;
        }

        public string GetFatalLines()
        {
            string strRetVal = "";
            foreach (DebugLine strItem in _lines)
            {
                if (strItem.IsFatal)
                {
                    strRetVal += strItem.Line;
                }
            }
            return strRetVal;
        }

        public string WriteLogFile()
        {
            string strRet = "";
            if (string.IsNullOrEmpty(_fn))
            {
                strRet = "No Logfile name set!";
            }
            else
            {
                string strFL = this.GetFatalLines();
                if (!string.IsNullOrEmpty(strFL))
                {
                    try
                    {
                        MailMessage mail = new MailMessage();
                        mail.Body = String.Format("[Logged at {0}]<br />\n{1}", _fn, strFL.Replace("\r\n", "<br />\n"));
                        mail.IsBodyHtml = true;
                        mail.To.Add(new MailAddress(ConfigurationManager.AppSettings["EmailTo"].ToString()));
                        mail.From = new MailAddress(ConfigurationManager.AppSettings["EmailFrom"].ToString());
                        mail.Subject = string.IsNullOrEmpty(this.DebugSubject.Trim()) ? String.Format("Lyris Sync error logged in {0}", System.IO.Path.GetFileName(_fn)) : String.Format("Lyris Sync error at {0}", this.DebugSubject.Trim());
                        mail.SubjectEncoding = Encoding.UTF8;
                        mail.Priority = MailPriority.Normal;
                        SmtpClient smtp = new SmtpClient();
                        if (ConfigurationManager.AppSettings["SmtpHostUser"] != null && ConfigurationManager.AppSettings["SmtpHostPass"] != null)
                        {
                            smtp.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SmtpHostUser"].ToString(), ConfigurationManager.AppSettings["SmtpHostPass"].ToString());
                        }
                        smtp.Host = ConfigurationManager.AppSettings["SmtpHost"].ToString();
                        smtp.Send(mail);
                    }
                    catch (Exception ex)
                    {
                        this.AddLine(String.Format("!Email Error Send Failed: {0}", ex.Message));
                    }
                }

                try
                {
                    using (System.IO.StreamWriter sw = new System.IO.StreamWriter(_fn, false))
                    {
                        sw.Write(this.GetLines());
                    }
                }
                catch (Exception ex)
                {
                    strRet = ex.Message.ToString();
                }
            }
            return strRet;
        }
    }
}
