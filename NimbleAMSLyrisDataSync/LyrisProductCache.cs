﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;


namespace NimbleAMSLyrisDataSync
{
    class LyrisProductCache
    {
        public Dictionary<string, LyrisProductCacheItem> Products;

        public LyrisProductCache(string SQLConnection, string SQLCommand)
        {
            RefreshCache(SQLConnection, SQLCommand);
        }

        public void RefreshCache(string SQLConnection, string SQLCommand)
        {
            if (this.Products == null)
            {
                this.Products = new Dictionary<string, LyrisProductCacheItem>();
            }
            else
            {
                this.Products.Clear();
            }
            using (SqlConnection sqlConn = new SqlConnection(SQLConnection))
            {
                SqlCommand sqlCmd = new SqlCommand(SQLCommand, sqlConn);
                sqlCmd.Connection.Open();
                SqlDataReader sdrInfo = sqlCmd.ExecuteReader();
                if (sdrInfo.HasRows)
                {
                    while (sdrInfo.Read())
                    {
                        this.Products.Add(SafeGetString(sdrInfo["NU__Product__c@Id"]), new LyrisProductCacheItem(
                            SafeGetString(sdrInfo["NU__Product__c@Name"]),
                            SafeGetString(sdrInfo["NU__Product__c@Online_Subscription_Type__c"]),
                            SafeGetString(sdrInfo["NU__Product__c@Lyris_List_Name__c"]),
                            SafeGetString(sdrInfo["NU__Product__c@List_Manager__r@PersonEmail"]),
                            SafeGetString(sdrInfo["NU__Product__c@List_Manager__r@FirstName"]),
                            SafeGetString(sdrInfo["NU__Product__c@List_Manager__r@LastName"]),
                            SafeGetString(sdrInfo["NU__Product__c@List_Manager__r@PersonTitle"]),
                            SafeGetString(sdrInfo["NU__Product__c@Welcome_Email_Body__c"]),
                            SafeGetString(sdrInfo["NU__Product__c@Welcome_Email_Subject__c"])
                            )
                        );
                    }
                }
                sqlCmd.Connection.Close();
            }
        }

        public string SafeGetString(object o)
        {
            if (o != null)
            {
                try
                {
                    return o.ToString();
                }
                catch
                {
                    return "";
                }
            }
            else
            {
                return "";
            }
        }
    }

    class LyrisProductCacheItem
    {
        /*
            {subscription_name}	(Wellness)
            {subscription_type}	(Listserv)

            {subscription_admin_email}	(blah@blah.com)
            {subscription_admin_first_name}	(Blah)
            {subscription_admin_last_name}	(Blah)
            {subscription_admin_job_title}	(Chief of Blah)

            {subscription_list_email}	(lyris_list_id_name@lyris.leadingage.org)
        */
        public string SubscriptionName { get; set; }
        public string SubscriptionType { get; set; }
        public string SubscriptionListEmail { get; set; }

        public string ListManagerEmail { get; set; }
        public string ListManagerFirstName { get; set; }
        public string ListManagerLastName { get; set; }
        public string ListManagerJobTitle { get; set; }

        public string WelcomeEmailBody { get; set; }
        public string WelcomeEmailSubject { get; set; }

        public LyrisProductCacheItem(string SubName, string SubType, string SubLEmail, string LMEmail, string LMFName, string LMLName, string LMJTitle, string WEBody, string WESubject)
        {
            this.SubscriptionName = SubName;
            this.SubscriptionType = SubType;
            this.SubscriptionListEmail = SubLEmail;

            this.ListManagerEmail = LMEmail;
            this.ListManagerFirstName = LMFName;
            this.ListManagerLastName = LMLName;
            this.ListManagerJobTitle = LMJTitle;

            if (SubLEmail == null || DBNull.Value.Equals(SubLEmail) || string.IsNullOrEmpty(SubLEmail))
            {
                this.SubscriptionListEmail = "reply";
            }

            if (!this.SubscriptionListEmail.Contains("@"))
            {
                this.SubscriptionListEmail += "@lyris.leadingage.org";
            }

            if (this.ListManagerEmail == null || DBNull.Value.Equals(this.ListManagerEmail) || string.IsNullOrEmpty(this.ListManagerEmail))
            {
                this.ListManagerEmail = "info@leadingage.org";
            }

            if (this.ListManagerFirstName == null || DBNull.Value.Equals(this.ListManagerFirstName) || string.IsNullOrEmpty(this.ListManagerFirstName))
            {
                this.ListManagerFirstName = "Info";
            }

            if (this.ListManagerLastName == null || DBNull.Value.Equals(this.ListManagerLastName) || string.IsNullOrEmpty(this.ListManagerLastName))
            {
                this.ListManagerLastName = "Address";
            }

            if (this.ListManagerJobTitle == null || DBNull.Value.Equals(this.ListManagerJobTitle) || string.IsNullOrEmpty(this.ListManagerJobTitle))
            {
                this.ListManagerJobTitle = "Customer Support";
            }

            this.WelcomeEmailBody = UpdateMergeField(WEBody);
            this.WelcomeEmailSubject = UpdateMergeField(WESubject);
        }

        internal string UpdateMergeField(string Field)
        {
            string retVal = Field;
            retVal = retVal.Replace("{subscription_name}", this.SubscriptionName);
            retVal = retVal.Replace("{subscription_type}", this.SubscriptionType);
            retVal = retVal.Replace("{subscription_list_email}", this.SubscriptionListEmail);
            retVal = retVal.Replace("{subscription_admin_email}", this.ListManagerEmail);
            retVal = retVal.Replace("{subscription_admin_first_name}", this.ListManagerFirstName);
            retVal = retVal.Replace("{subscription_admin_last_name}", this.ListManagerLastName);
            retVal = retVal.Replace("{subscription_admin_job_title}", this.ListManagerJobTitle);
            return retVal;
        }
    }
}
